#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QLabel>
#include <QtSql>
#include <iostream>

using namespace std;
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    this->connexion = new Form_connexion(this);
    connexion->exec();


}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::goInscription ( void ) {
    this->connexion->close();

    this->inscription = new Form_inscription(this);
    inscription->exec();
}

void MainWindow::goConnection ( void ) {
    this->inscription->close();

    this->connexion = new Form_connexion(this);
    connexion->exec();
}

void MainWindow::doneConnexion(int ID) {
    this->connexion->close();

    this->init(ID);
}

void MainWindow::init(int ID) {
    QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL");

    db.setHostName( "localhost" );
    db.setUserName( "dev-cpp" );
    db.setPassword( "Djn64jb.cpp!" );
    db.setDatabaseName( "intranet_simplon" );

    if( db.open() ) {
        cout << "Vous êtes maintenant connecté à " << db.hostName().toStdString() << endl;
    }
    else {
        // @todo: Gérer l'affichage graphique de l'erreur
        cout << "### ERREUR, IMPOSSIBLE A SE CONNECTER A LA BASE DE DONNEES ###";
        return;
    }

    QSqlQuery query;

    if( query.exec("SELECT * FROM user") ) {

        QLabel *id, *nom, *prenom, *email;
        int n = 0; // compteur de ligne

        while(query.next()) {
            id = new QLabel( query.value( 0 ).toString() );
            nom = new QLabel( query.value( 1 ).toString() );
            prenom = new QLabel( query.value( 2 ).toString() );
            email = new QLabel( query.value( 3 ).toString() );

            this->ui->liste_membre->addWidget( id, n, 0 );
            this->ui->liste_membre->addWidget( nom, n, 1 );
            this->ui->liste_membre->addWidget( prenom, n, 2 );
            this->ui->liste_membre->addWidget( email, n, 3 );

            n++;
        }
    }
}
