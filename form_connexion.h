#ifndef FORM_CONNEXION_H
#define FORM_CONNEXION_H

#include <QDialog>

namespace Ui {
class Form_connexion;
}

class Form_connexion : public QDialog
{
    Q_OBJECT

public:
    explicit Form_connexion(QWidget *parent = 0);
    ~Form_connexion();

public slots:
    void try_to_connect( void );

signals:
    void s_done_connexion( int ID );

private:
    Ui::Form_connexion *ui;
};

#endif // FORM_CONNEXION_H
