#include "validation.h"

#include <string>
#include <iostream> // Pour les cout

#include <QRegExp>

using namespace std;


Validation::Validation( QString value ) {
    this->value = value;
}


void Validation::exeTest( void ) {
    // On itère sur la map test_todo
    for ( map<int, int>::iterator it_test_todo = test_todo.begin() ; it_test_todo != test_todo.end() ; ++it_test_todo ) {

    }

    // On vide le test_todo
    this->test_todo.clear();

}

void Validation::addTest( int code_test, int arg ) {
    switch ( code_test ) {
        case T_ONLY_LETTERS:
            this->only_letters();
            break;

        case T_ONLY_LETTERS_OR_NUMBERS:
            this->only_letters_or_numbers();
            break;

        case T_LENGHT_MIN:
            this->lenght_min( arg );
            break;

        case T_LENGHT_MAX:
            this->lenght_max( arg );
            break;

        case T_EMAIL_FORMAT:
            this->email_format();
            break;

        case T_AT_LEAST_CAPITAL:
            this->at_least_capital();
            break;

        case T_AT_LEAST_NUMBER:
            this->at_least_number();
            break;

        default:
            cout << "\nATTENTION : TEST #" << code_test << " EST INCONNUS" ;
            break;
    }
}

void Validation::addTest( int code_test, QString arg ) {
    switch ( code_test ) {
        case T_EQUALITY:
            this->equality( arg );
            break;

        default:
            cout << "\nATTENTION : TEST #" << code_test << " EST INCONNUS" ;
            break;
    }
}

void Validation::setValue( QString value ) {
    this->value = value;
}

map<int, bool> Validation::getError( void ) {
    return this->code_error;
}

void Validation::only_letters( void ) {
    cout << "\nTest #" << T_ONLY_LETTERS << " with (value = '" << this->value.toStdString() << "')" ;

    QRegExp re("^[A-Za-zéèçàêë-]+$");

    if ( ! re.exactMatch( this->value ) ) {
        this->error( T_ONLY_LETTERS );
    }
    else {
        this->success( T_ONLY_LETTERS );
    }
}
void Validation::only_letters_or_numbers( void ) {
    cout << "\nTest #" << T_ONLY_LETTERS_OR_NUMBERS << " with (value = '" << this->value.toStdString() << "')" ;

    QRegExp re("^[A-Za-z0-9éèçàêë-]+$");

    if ( ! re.exactMatch( this->value ) ) {
        this->error( T_ONLY_LETTERS_OR_NUMBERS );
    }
    else {
        this->success( T_ONLY_LETTERS_OR_NUMBERS );
    }
}
void Validation::email_format( void ) {
    cout << "\nTest #" << T_EMAIL_FORMAT << " with (value = '" << this->value.toStdString() << "')";

    QRegExp re("^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$");

    if ( ! re.exactMatch( this->value ) ) {
        this->error( T_EMAIL_FORMAT );
    }
    else {
        this->success( T_EMAIL_FORMAT );
    }
}
void Validation::at_least_capital( void ) {
    cout << "\nTest #" << T_AT_LEAST_CAPITAL << " with (value = '" << this->value.toStdString() << "')";

    QRegExp re("[A-Z]+");

    if ( ! re.exactMatch( this->value ) ) {
        this->error( T_AT_LEAST_CAPITAL );
    }
    else {
        this->success( T_AT_LEAST_CAPITAL );
    }
}
void Validation::at_least_number( void ) {
    cout << "\nTest #" << T_AT_LEAST_NUMBER << " with (value = '" << this->value.toStdString() << "')";

    QRegExp re("[0-9]+");
    bool var = re.exactMatch( this->value );
    if ( ! var ) {
        this->error( T_AT_LEAST_NUMBER );
    }
    else {
        this->success( T_AT_LEAST_NUMBER );
    }
}
void Validation::lenght_min( int lenghtMin ) {
    cout << "\nTest #" << T_LENGHT_MIN << " with (value = '" << this->value.toStdString() << "' et lenghtMin = '" << lenghtMin << "')" ;

    if ( this->value.size() < lenghtMin ) {
        this->error( T_LENGHT_MIN );
    }
    else {
        this->success( T_LENGHT_MIN );
    }
}
void Validation::lenght_max( int lenghtMax ) {
    cout << "\nTest #" << T_LENGHT_MAX << " with (value = '" << this->value.toStdString() << "' et lenghtMax = '" << lenghtMax << "')" ;

    if ( this->value.size() > lenghtMax ) {
        cout << "\nLa chaine est trop longue";
        this->error( T_LENGHT_MAX );
    }
    else {
        this->success( T_LENGHT_MAX );
    }
}

void Validation::equality( QString val ) {
    cout << "\nTest #" << T_EQUALITY << " with (value = '" << this->value.toStdString() << "', et val = '" << val.toStdString() << "')" ;

    if ( this->value != val ) {
        this->error( T_EQUALITY );
    }
    else {
        this->success( T_EQUALITY );
    }
}


void Validation::success( int codeTest ) {
    this->code_error[ codeTest ] = false;
    cout << " => Test Réussie !";
}
void Validation::error( int codeTest ) {
    this->code_error[ codeTest ] = true;
    cout << " => Test échoué...";
}
