#-------------------------------------------------
#
# Project created by QtCreator 2017-09-02T14:05:51
#
#-------------------------------------------------

QT       += core gui
QT += sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = intranet_simplon
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    form_inscription.cpp \
    validation.cpp \
    form_connexion.cpp

HEADERS  += mainwindow.h \
    form_inscription.h \
    validation.h \
    form_connexion.h

FORMS    += mainwindow.ui \
    form_inscription.ui \
    form_connexion.ui
